﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {


    public GameObject kiwi;

    public GameObject _GM;

    Transform camera;
    BoxCollider2D col;
    bool flag = true;

    void Start()
    {
        _GM = GameObject.Find("_GM");
        camera = gameObject.GetComponent<Transform>();
        col = gameObject.GetComponent<BoxCollider2D>();
        float size = gameObject.GetComponent<Camera>().orthographicSize;
        col.size = new Vector2(size / 2, size / 2);
        col.offset = new Vector2(0, 2);
    }

	void Update () {
        if (flag)
        {
            kiwi = GameObject.Find("kiwi(Clone)");
            flag = false;
        }
        camera.position = new Vector3(kiwi.GetComponent<Transform>().position.x, camera.position.y, -10);
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        //Debug.Log("Continue");
        if (coll.gameObject.tag == "cloud")
        {
            _GM.GetComponent<GameController>().CloudDelete();
        }
    }
}
