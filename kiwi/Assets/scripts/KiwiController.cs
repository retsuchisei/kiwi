﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KiwiController : MonoBehaviour {

    public GameObject _GM;
    public Transform ground;

    public LayerMask layerMask;

    public float speed;
    public float jumpForceX;
    public float jumpForceY;

    public bool isGrounded;

    Rigidbody2D rb;
    float pos = 0;

    int count = 0;

    public KeyCode jump;

    Animator anim;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
        _GM = GameObject.Find("_GM");
        anim = GetComponent<Animator>();
    }
	
	void FixedUpdate () {

        anim.SetBool("jump", false);
        isGrounded = Physics2D.Linecast(transform.position, ground.position, layerMask);
        anim.SetBool("isGrounded", isGrounded);

        if (isGrounded)
        {

            rb.velocity = new Vector3(-speed, 0);

            if (Input.GetKeyUp(jump))
            {
                anim.SetBool("jump", true);
                anim.SetBool("isGrounded", false);  
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-jumpForceX, jumpForceY));
            }
        } else
        {
            if (rb.velocity.x > -speed / 2)
            {
                rb.velocity = new Vector3(-speed / 2, 0);
            }
        }

        if (Mathf.Abs(GetComponent<Transform>().position.x - pos) >= 1)
        {
            pos = GetComponent<Transform>().position.x;
            _GM.GetComponent<GameController>().GrassContinue();
            ++count;
        }

	}

   /* void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "grass")
        {
            _GM.GetComponent<GameController>().GrassContinue();
        }
    }*/

}
