﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public GameObject[] grass = new GameObject[2];
    public GameObject[] cloud = new GameObject[3];
    public GameObject[] backBlocks = new GameObject[2];
    public GameObject stone;
    public GameObject kiwi;

    int blockCount = -10;

    int obstacleCount = 0;

    Queue<GameObject> grassQueue = new Queue<GameObject>();
    Queue<GameObject> cloudQueue = new Queue<GameObject>();

    GameObject kiwiG;

	void Start () {
        kiwiG = Instantiate(kiwi, new Vector3(0, -0.5f), Quaternion.identity);
        for (int i = -10; i < 25; ++i)
        {
            GameObject grassInst = Instantiate(grass[0], new Vector3(-(blockCount++), -1), Quaternion.identity);
            grassQueue.Enqueue(grassInst);
            if ((int)Random.Range(0, 5) == 2)
            {
                GameObject cloudInst = Instantiate(cloud[(int)Random.Range(0, 3)], new Vector3(-(blockCount), 3), Quaternion.identity);
                cloudQueue.Enqueue(cloudInst);
            }
            //Debug.Log(blockCount);
        }

    }
	
	// Update is called once per frame
	void Update () {
        GameObject tempGrass = grassQueue.Peek();
        //Debug.Log(tempGrass.GetComponent<Transform>().position.x - kiwiG.GetComponent<Transform>().position.x);
        if (Mathf.Abs(tempGrass.GetComponent<Transform>().position.x - kiwiG.GetComponent<Transform>().position.x) > 20)
        {
            //Debug.Log("SSSS");
            GrassContinue();
        }
	}

    public void GrassContinue()
    {
        GameObject grassInst;
        if (Random.Range(0, 16) == 8 && obstacleCount < 0)
        {
            grassInst = Instantiate(grass[1], new Vector3(-(blockCount++), -1), Quaternion.identity);
            obstacleCount = 3;
        }
        else
        {
            grassInst = Instantiate(grass[0], new Vector3(-(blockCount++), -1), Quaternion.identity);
            int rand = Random.Range(0, 6);
            if (rand == 0)
            {
                GameObject tempBack = Instantiate(backBlocks[0], new Vector3(-(blockCount - 1), (float)(0.72 - 1.0)), Quaternion.identity);
                tempBack.transform.parent = grassInst.transform;
            } else if (rand == 1)
            {
                GameObject tempBack = Instantiate(backBlocks[1], new Vector3(-(blockCount - 1), (float)(1.73 - 1.0)), Quaternion.identity);
                tempBack.transform.parent = grassInst.transform;
            }
            if (Random.Range(0, 16) == 8 && obstacleCount < 0)
            {
                obstacleCount = 3;
                GameObject tempStone = Instantiate(stone, new Vector3(-(blockCount - 1), (float)(0.42 - 1.0)), Quaternion.identity);
                tempStone.transform.parent = grassInst.transform;
            }

        }
        grassQueue.Enqueue(grassInst);
        Destroy(grassQueue.Dequeue(), 2);
        if ((int)Random.Range(0, 5) == 2)
        {
            GameObject cloudInst = Instantiate(cloud[(int)Random.Range(0, 2)], new Vector3(-(blockCount), 3), Quaternion.identity);
            cloudQueue.Enqueue(cloudInst);
        }

        --obstacleCount;
        
        //Debug.Log(grassQueue.Count);
    }

    public void CloudDelete()
    {
        Destroy(cloudQueue.Dequeue(), 3);
    }
}
